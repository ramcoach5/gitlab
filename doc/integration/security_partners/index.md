---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
type: index
---

# Security partner integrations

You can integrate GitLab with its security partners. This page has information on how do this with
each security partner:

- [Anchore](https://docs.anchore.com/current/docs/using/integration/ci_cd/gitlab/)
